var infowindow = null;
var markerobj = [];
var thurstonLat = jQuery('#map').attr('data-thurston-lat');
var thurstonLng = jQuery('#map').attr('data-thurston-lng');

var myLatlng = new google.maps.LatLng(thurstonLat, thurstonLng);

jQuery(document).ready(function () {
    initialize();
});

function initialize() {

     var styles = [{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#c0c2c3"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#dde0e4"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#96af64"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#d3a64f"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#9ac8e6"}]},{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"transit.station","elementType":"labels.text","stylers":[{"visibility":"off"}]}];


    // create map
    var centerMap = new google.maps.LatLng(0, 0);
    var myOptions = {
        zoom: 14,
        minZoom: 1,
        maxZoom: 20,
        center: centerMap,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: styles,
        scrollwheel: false,
    }


    //attacth map to #map
    var map = new google.maps.Map(document.getElementById("map"), myOptions);

    // set the site markers and recenter the map
    setMarkers(map, sites, infowindow);


    var thurstonMapMarker = jQuery('#map').attr('data-map-marker');
    var thurstonMarker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Maplewood Place',
        icon: thurstonMapMarker
    });
    //recenter map on resize
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
    infowindow = new google.maps.InfoWindow({
        content: "loading...",
        // maxWidth: 0,
        boxStyle: {
           whiteSpace: "nowrap"
        }
    });
    // check what current tab is and adjust visable markers
    var default_location_type = jQuery('#map-filters').find('.catFilter.flex-active').attr('data-filter-name');
    adjustMapMarkers(default_location_type, map);

    // when new tab is selected, filter current visable markers
    jQuery('#map-filters').on('click', '.catFilter', function () {
        filter = jQuery(this).attr('data-filter-name');
        color = jQuery(this).attr('data-filter-color');
        jQuery('#neighbourhood-map').css('background',color);
        jQuery(this).parent().parent().find('.catFilter').css('background','white');
        jQuery(this).css('background',color).parent().find('.content').css('background',color);


        if(jQuery(this).hasClass('title')){
            jQuery('#map-filters').find('li.catFilter').removeClass('flex-active').parent().find('[data-filter-name='+filter+']').click().addClass('flex-active');
            jQuery('#map-filters').find('.slides').find('[data-filter-name='+filter+']').addClass('flex-active-slide').siblings().removeClass('flex-active-slide');
        }else{
            jQuery('#map-filters .accordion').find('.flex-active').find('.title').css('background','white').parent().removeClass('flex-active').parent().find('[data-filter-name='+filter+']').parent().addClass('flex-active');

            jQuery('#map-filters .accordion').find('[data-filter-name='+filter+']').css('background',color).siblings().css('background',color);
        }

        // jQuery(this).css('background',color).siblings().css('background','white');
        infowindow.close();
        adjustMapMarkers(filter, map); 


    });

    // when single location is clicked in tab, display the popup on the map
    jQuery('#map-filters').on('click', '.single-location', function (e) {
        var location_id = jQuery(this).attr('data-location-count');
        // e.preventDefault();
        var placeIdAttr;
        var placeId;
        placeId = jQuery(this).attr('data-location-count');
        google.maps.event.trigger(markerobj[placeId],"click");
        console.log(placeId);
    });
}

function adjustMapMarkers(filter, map){

    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < sites.length; i++) {
        
        if(sites[i][5] == filter) {
            markerobj[i].setVisible(true);
            bounds.extend(markerobj[i].position);

        } else {
            markerobj[i].setVisible(false);
        }
    }
    bounds.extend(myLatlng);

    map.fitBounds(bounds);
}

function setMarkers(map, markers) {

    // var infowindow = new google.maps.InfoWindow();  

    markerobj = [];
    for (var i = 0; i < markers.length; i++) {
        var sites = markers[i];
        var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);
        markerobj[i] = new google.maps.Marker({
            position: siteLatLng,
            map: map,
            title: sites[0],
            zIndex: sites[3],
            html: sites[4],
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 4,
                strokeColor : sites[6],
                strokeWeight : 5,
                fillOpacity: 1,
                fillColor: sites[6]
            },
        });

        var contentString = "";
        google.maps.event.addListener(markerobj[i], "click", function () {
            infowindow.close();
            infowindow.setContent(this.html);
            infowindow.open(map, this);

        });
    }
}


google.maps.event.addDomListener(window, 'load', initialize);
