jQuery(document).ready(function($) {

	$(".mobile-trigger, .mobile-trigger + .menu-slide").click(function(e){
		e.preventDefault();

		$('.mobile-trigger').toggleClass('open');
		$('.mobile-nav-container').stop(true, true).slideToggle(200);
	});

	$("#mobile-nav > li").find('.sub-menu').parent().children('a').after('<span class="menu-slide"></span>');

	$("#mobile-nav > li .menu-slide").on('click',function(e){
		$(this).toggleClass('open');
		e.preventDefault();
		e.stopPropagation();

		$(this).siblings('.sub-menu').stop(true, true).slideToggle(200);
	});

	$('.slider').flexslider({
		animation: "fade",
		directionNav: false,
	});

	$.localScroll();

	$('.neighbourhoods-link').on('click',function(){
		$('#top-banner-flydown').slideToggle();
	});

	$('.tab-slider').flexslider({
		animation: "fade",
		manualControls: ".slider-thumbs li",
		animationSpeed: 500,
		// controlNav: false,
		directionNav: false,
		slideshow: false
	});

	$('.fancybox').fancybox({
		'width'		: 300,
		'autoWidth' : false,
	});


	jQuery(".fancy-floorplan").fancybox({
		'scrolling'		: 'yes',
		'titleShow'		: false,
		'width'  : 1160, 
		'topRatio' : 0,
		'autoWidth': false,
	    'autoSize':false,
	    'autoHeight' : true,
	});


	// Watch for clicks of the accordion titles
	$( ".accordion .title" ).click(function() {
		// Check if this area is active or not
		if($(this).parent().hasClass("flex-active")){
			// Close our current area
			$(this).parent().removeClass("flex-active");
		}else{
			// Close all open areas
			// Comment out the line below if you want your accordion items to stay open until they are clicked closed
			$(this).closest('.accordion').find('.flex-active').removeClass('flex-active'); 

			// Open our current accordion area
			$(this).parent().addClass("flex-active");
		}
	});

});
