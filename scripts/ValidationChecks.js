﻿        var IE = document.all ? true : false
		var counter = 0;

        // If NS -- that is, !IE -- then set up for mouse capture
        if (!IE) document.captureEvents(Event.MOUSEMOVE)

        // Set-up to use getMouseXY function onMouseMove
        document.onmousemove = getMouseXY;

        // Temporary variables to hold mouse x-y pos.s
        var tempX = 0
        var tempY = 0

        // Main function to retrieve mouse x-y pos.s

        function getMouseXY(e) {
			try{
				
				if (IE) { // grab the x-y pos.s if browser is IE
					tempX = event.clientX + document.body.scrollLeft
					tempY = event.clientY + document.body.scrollTop
				} else {  // grab the x-y pos.s if browser is NS
					tempX = e.pageX
					tempY = e.pageY
				}
				// catch possible negative values in NS4
				if (tempX < 0) { tempX = 0 }
				if (tempY < 0) { tempY = 0 }
				// show the position values in the form named Show
				// in the text fields named MouseX and MouseY
				form1.x.value = tempX
				form1.y.value = tempY
				return true
			}
			catch(ex){
				return false;
			}
        }

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
}

String.prototype.empty = function() {
    if (this.length == 0)
        return true;
    else if (this.length > 0)
        return /^\s*$/.test(this);
}

function doClick(){
	counter++;
	if (counter > 1)
		return false;
	return true;
}

/* Client-side access to querystring name=value pairs
	Version 1.3
	28 May 2008
	
	License (Simplified BSD):
	http://adamv.com/dev/javascript/qslicense.txt
*/
function Querystring(qs) { // optionally pass a querystring to parse
	this.params = {};
	
	if (qs == null) qs = location.search.substring(1, location.search.length);
	if (qs.length == 0) return;

// Turn <plus> back to <space>
// See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
	qs = qs.replace(/\+/g, ' ');
	var args = qs.split('&'); // parse out name/value pairs separated via &
	
// split out each name=value pair
	for (var i = 0; i < args.length; i++) {
		var pair = args[i].split('=');
		var name = decodeURIComponent(pair[0]);
		
		var value = (pair.length==2)
			? decodeURIComponent(pair[1])
			: name;
		
		this.params[name] = value;
	}
}

Querystring.prototype.get = function(key, default_) {
	var value = this.params[key];
	return (value != null) ? value : default_;
}

Querystring.prototype.contains = function(key) {
	var value = this.params[key];
	return (value != null);
}


function tooLong(txtBox, iLength, errorSpan) {
    var errorTxt = "";
    
    if (txtBox.value.length > iLength) {
        txtBox.style.background = 'Yellow';
        errorTxt = "Must be less than " + iLength.toString() + " characters long.";
    }
    errorSpan.innerHTML = errorTxt;
}
function pReq(pList){
	if(pList.value == "-1"){
		alert("Make a selection in drop down.");
		pList.style.backgroundColor = 'Yellow';
		return false;
	}
	else{
		pList.style.border = 'White';
		return true;
	}
}
function cReq(cBox){
if(cBox.checked){
cBox.style.backgroundColor = 'White';
return true;
}
cBox.style.backgroundColor = 'Yellow';
alert("Must select checkbox");
return false;
}
function rReq(rBut1, rBut2){
	if(rBut1.checked == false && rBut2.checked == false){
	alert("Must make a selection.");
	rBut1.style.backgroundColor = 'Yellow';
	rBut2.style.backgroundColor = 'Yellow';
	return false;
	}
	rBut1.style.backgroundColor = 'White';
	rBut2.style.backgroundColor = 'White';
	return true;
}
function integerCheck(txtBox, iMinValue, iMaxValue, errorSpan) {
    var errorTxt = "";

    if ( iMinValue > iMaxValue ) {
	var iHold = iMinValue;
	iMinValue = iMaxValue;
	iMaxValue = iHold;
    }
    txtBox.style.background = 'White';
    txtBox.value = txtBox.value.replace(new RegExp("\\$", "g"), "");

    if (txtBox.value.length > 0) {
        var numEx = new RegExp("^[-+]?[0-9]*$");
        if (txtBox.value.match(numEx)) {
            var iVal = parseInt(txtBox.value, 10);
            if (iVal > iMaxValue) {
                txtBox.style.background = 'Yellow';
                errorTxt += "The Maximum Value allowed is: " + iMaxValue.toString() + "\n";
            }
            if (iVal < iMinValue) {
                txtBox.style.background = 'Yellow';
                errorTxt += "The Minimum Value allowed is: " + iMinValue.toString() + "\n";
            }
        }
        else {
            txtBox.style.background = 'Yellow';
            errorTxt += "Must be an integer value.\n";
        }
    }

    if(errorTxt != ""){
	alert(errorTxt);
	txtBox.value = "";
    }
}
function floatCheck(txtBox, iMinValue, iMaxValue, iPrecision, errorSpan) {
    var errorTxt = "";

    if (iMinValue > iMaxValue) {
        var iHold = iMinValue;
        iMinValue = iMaxValue;
        iMaxValue = iHold;
    }

    txtBox.style.background = 'White';
    txtBox.value = txtBox.value.replace(new RegExp("\\$", "g"), "");
    if (txtBox.value.length > 0) {
		txtBox.value = txtBox.value.replace(/£/g, "");
		txtBox.value = txtBox.value.replace(/$$/g, "");
        var numEx = new RegExp("[-+]?[0-9]*\.?[0-9]+");//"^[-+]?[0-9]*[.]?([0-9]*)$"
        if (txtBox.value.match(numEx)) {
            var fVal = parseFloat(txtBox.value);
            var precExp = new RegExp("[-+]?[0-" + iPrecision.toString() + "]*\.?[0-9]+");//^[-+]?[0-9]*[.]?([0-9]{0," + iPrecision.toString() + "})$"
            if (txtBox.value.match(precExp)) {
                if (fVal > iMaxValue) {
                    errorTxt += "The Maximum Value allowed is: " + iMaxValue.toString() + "\n";
                }
                if (fVal < iMinValue) {
                    errorTxt += "The Minimum Value allowed is: " + iMinValue.toString() + "\n";
                }
            }
            else {
                errorTxt += "Value can only have " + iPrecision.toString() + " points of precision.\n";
            }
        }
        else {
            errorTxt += "Value must be a valid floating point number.\n";
        }
    }

    if (errorTxt != "") {
        txtBox.style.background = 'Yellow';
        alert(errorTxt);
        txtBox.value = "";
    }
}

function checkEmail(txtBox) {
    var emailEx = new RegExp("^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$");

    if (!txtBox.value.match(emailEx)) {
        txtBox.value = "";
        txtBox.style.background = 'Yellow';
        alert('Please provide a valid Email address');
		return false;
    }
    else {
        txtBox.style.background = 'White';
		return true;
    }
}

function blackListed(txtbox) {
	var formId = document.getElementById('formID');
	if (formId == null){
		var form = document.getElementById('form1');
		formId = new Object;
		formId.value = form.action.split('=')[1];
		if (formId.value == null)
		return;
	}
	var pattern = txtbox.value;
	if (pattern == null || pattern == "")
		return;
	var parts = pattern.split('@');

	if (parts.length > 1)
		pattern = parts[1];

	var req = window.XDomainRequest ? new window.XDomainRequest() : new XMLHttpRequest();
	req.txtBox = txtbox;
	if (window.XDomainRequest){
		req.onload = function() {
			if (req.responseText == "True") {
				req.txtBox.value = "";
				req.txtBox.style.background = 'Yellow';
				req.txtBox.focus();
				alert("The supplied email address is invalid. Please use a corporate email address.");
			}
			else {
				req.txtBox.style.background = 'White';
			}
		}
	}
	else{
		req.onreadystatechange = function () {
			if (req.readyState == 4) {
				if (req.status == 200) {
					if (req.responseText == "True") {
						req.txtBox.value = "";
						req.txtBox.style.background = 'Yellow';
						req.txtBox.focus();
						alert("The supplied email address is invalid. Please use a corporate email address.");
					}
					else {
						req.txtBox.style.background = 'White';
					}
				}
				else {
					req.txtBox.value = "";
					req.txtBox.style.background = 'Yellow';
					req.txtBox.focus();
					alert("There was an error validating the suplied email address. Please use a corporate email address.");
				}
			}
		}
	}
	try{
		if (window.location.protocol == "http:")
			req.open("GET", "http://web2crm.crminnovation.com/blacklist.ashx?formId=" + formId.value + "&pattern=" + encodeURI(pattern), true);
		else
			req.open("GET", "https://web2crm.crminnovation.com/blacklist.ashx?formId=" + formId.value + "&pattern=" + encodeURI(pattern), true);
		req.send();
	}
	catch(e){
	}
}

function checkUrl(txtBox) {
    var urlEx = new RegExp("(http://)|(https://)(.*)");
    if (!txtBox.value.match(urlEx)) {
        txtBox.value = "http://" + txtBox.value;
    }
}


function generateCaptcha(first, second) {
            first.innerHTML = Math.floor(Math.random() * 10);
            second.innerHTML = Math.floor(Math.random() * 10);
        }
        function checkCaptcha(first, second, answer) {
            try {
                var iFirst = parseInt(first.innerHTML, 10);
                var iSecond = parseInt(second.innerHTML, 10);
                var iAnswer = parseInt(answer.value, 10);

                var realAnswer = iFirst + iSecond;
                if (realAnswer != iAnswer) {
                    answer.value = "";
                    answer.style.background = 'Yellow';
                    alert('Wrong answer to Security Code');
                    generateCaptcha(first, second);
                    return false;
                }
                else {
                    answer.style.background = 'White';
                    return true;
                }
            }
            catch (e) {

            }
        }
		
function DateCheck(txtBox) {
    var errorTxt = "";

    var dateTimeRegEx = new RegExp(/^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/);  //RegExp("^(([0]?[1-9]{1,1})|([1]{1,1}[1-2]{1,1}))[-|/]{1,1}(([0]?[1-9]{1,1})|([1]{1,1}[0-9]{1,1})|([2]{1,1}[0-9]{1,1})|([3]{1,1}[0-1]{1,1}))[-|/]{1,1}(((18)|(19)|((2)[0-9]))[0-9][0-9])?$");
    if (txtBox.value.trim().match(dateTimeRegEx)) {
        var dateArray = new Array();
        dateArray = txtBox.value.split('/');
        if (dateArray.length == 1) {
            dateArray = txtBox.value.split('-');
        }
        var adjustedMonth = parseInt(dateArray[0], 10);
        adjustedMonth = adjustedMonth - 1;
        var testDate = new Date(dateArray[2], adjustedMonth.toString(), dateArray[1]);

        if ((dateArray[1] == testDate.getDate()) && (adjustedMonth == testDate.getMonth()) && (dateArray[2] == testDate.getFullYear())) {
            txtBox.style.background = 'White';
        }
        else {
            errorTxt = "The Date Entered was not valid";
        }

    }
    else {
        //errorTxt = "Enter Date in a MM/DD/YYYY format";
			dateTimeRegEx = new RegExp(/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/);
			if (txtBox.value.trim().match(dateTimeRegEx)) {
			var dateArray = new Array();
			dateArray = txtBox.value.split('/');
			if (dateArray.length == 1) {
				dateArray = txtBox.value.split('-');
			}
			var adjustedMonth = parseInt(dateArray[0], 10);
			adjustedMonth = adjustedMonth - 1;
			var testDate = new Date(dateArray[2], adjustedMonth.toString(), dateArray[1]);

			if ((dateArray[1] == testDate.getDate()) && (adjustedMonth == testDate.getMonth()) && (dateArray[2] == testDate.getFullYear())) {
				txtBox.style.background = 'White';
			}
			else {
				errorTxt = "The Date Entered was not valid";
			}
		}
		else
			errorTxt = "Enter Date in a MM/DD/YYYY or DD/MM/YYYY format";

    }

    if (errorTxt != "") {
        txtBox.style.background = 'Yellow';
        alert(errorTxt);
        txtBox.value = "";
    }
}
		
/*function DateCheck(txtBox) {
    var errorTxt = "";

    var dateTimeRegEx = RegExp("^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$");//"^(([0]?[1-9]{1,1})|([1]{1,1}[1-2]{1,1}))[-|/]{1,1}(([0]?[1-9]{1,1})|([1]{1,1}[0-9]{1,1})|([2]{1,1}[0-9]{1,1})|([3]{1,1}[0-1]{1,1}))[-|/]{1,1}(((18)|(19)|((2)[0-9]))[0-9][0-9])?$");
    if (txtBox.value.match(/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/)) {
        var dateArray = new Array();
        dateArray = txtBox.value.split('/');
        if (dateArray.length == 1) {
            dateArray = txtBox.value.split('-');
        }
        var adjustedMonth = parseInt(dateArray[0], 10);
        adjustedMonth = adjustedMonth - 1;
        var testDate = new Date(dateArray[2], adjustedMonth.toString(), dateArray[1]);

        if ((dateArray[1] == testDate.getDate()) && (adjustedMonth == testDate.getMonth()) && (dateArray[2] == testDate.getFullYear())) {
            txtBox.style.background = 'White';
        }
        else {
		
			dateArray = txtBox.value.split('/');
			if (dateArray.length == 1) {
				dateArray = txtBox.value.split('-');
			}
			adjustedMonth = parseInt(dateArray[1], 10);
			adjustedMonth = adjustedMonth - 1;
			testDate = new Date(dateArray[2], adjustedMonth.toString(), dateArray[0]);
			if ((dateArray[0] == testDate.getDate()) && (adjustedMonth == testDate.getMonth()) && (dateArray[2] == testDate.getFullYear())) {
            txtBox.style.background = 'White';
			}	
			else{
				errorTxt = "The Date Entered was not valid";
			}
        }

    }
    else {
        errorTxt = "Enter Date in a MM/DD/YYYY format";
    }

    if (errorTxt != "") {
        txtBox.style.background = 'Yellow';
        alert(errorTxt);
        txtBox.value = "";
    }
}*/
function hideCalendars(calendarHolder) {
    var allCal = calendarHolder.childNodes;
    var calCount = allCal.length;
    for( var i=0; i<calCount; i++){
        allCal[i].style.visibility = 'hidden';
    }
}
function IsRequired(txtBox) {
    if (txtBox.value.trim() == "") {
	alert("Field is required");
	txtBox.style.background = 'Yellow';
	return false;
    }
    else {
        txtBox.style.background = 'White';
    }

    return true;
}
function FillHiddenValues(submitForm) {
    var qs = new Querystring();
    for (i = 0; i < submitForm.elements.length; i++) {
        if (submitForm.elements[i].type == "hidden") {
            var qsValue = qs.get(submitForm.elements[i].name);

            if (qsValue != undefined) {
                submitForm.elements[i].value = qsValue;
            }
            
        }
    }
}
function ValidateTextCheck(tBox1,tBox2){
	if(tBox1.value != tBox2.value){
		tBox1.style.background = 'Yellow';
		tBox2.style.background = 'Yellow';
		alert("Values must match");
		return false;
	}
	else{
		tBox1.style.background = 'White';
		tBox2.style.background = 'White';
		return true;		
	}
}
function cvs(){
	try{
		var vState = document.getElementById("__VIEWSTATE");
		if (vState != null){
			vState.parentNode.removeChild(vState);
		}
	}
	catch(e){}
}
function csv(){
	try{
		var vState = document.getElementById("__VIEWSTATE");
		vState.parentNode.removeChild(vState);
	}
	catch(e){}
}

function fileChecker(){
return true;
}

function readCookie(c)
{
    var cookie_array = document.cookie.split(';');
    var keyvaluepair = {};
    for (var cookie = 0; cookie < cookie_array.length; cookie++)
    {
        var key = cookie_array[cookie].substring(0, cookie_array[cookie].indexOf('=')).trim();
        var value = cookie_array[cookie].substring(cookie_array[cookie].indexOf('=')+1, cookie_array[cookie].length).trim();
        keyvaluepair[key] = value;
    }
 
    if (c)
        return keyvaluepair[c] ? keyvaluepair[c] : null;
 
    return keyvaluepair;
}

function gaCookie()
{
    var utma = function() {
        var utma_array;
 
        if (readCookie('__utma'))
            utma_array =  readCookie('__utma').split('.');
        else
            return null;
 
        var domainhash = utma_array[0];
        var uniqueid = utma_array[1];
        var ftime = utma_array[2];
        var ltime = utma_array[3];
        var stime = utma_array[4];
        var sessions = utma_array[5];
 
        return {
            'cookie': utma_array,
            'domainhash': domainhash,
            'uniqueid': uniqueid,
            'ftime': ftime,
            'ltime': ltime,
            'stime': stime,
            'sessions': sessions
        };
    };
 
    var utmb = function() {
        var utmb_array;
 
        if (readCookie('__utmb'))
            utmb_array = readCookie('__utmb').split('.');
        else
            return null;
        var gifrequest = utmb_array[1];
 
        return {
            'cookie': utmb_array,
            'gifrequest': gifrequest
        };
    };
 
    var utmv = function() {
        var utmv_array;
 
        if (readCookie('__utmv'))
            utmv_array = readCookie('__utmv').split('.');
        else
            return null;
 
        var value = utmv_array[1];
 
        return {
            'cookie': utmv_array,
            'value': value
        };
    };
	
    var utmz = function() {
        var utmz_array, source, medium, name, term, content, gclid;
 
        if (readCookie('__utmz'))
            utmz_array = readCookie('__utmz').split('.');
        else
            return null;
 
        var utms = utmz_array[4].split('|');
        for (var i = 0; i < utms.length; i++) {
            var key = utms[i].substring(0, utms[i].indexOf('='));
            var val = decodeURIComponent(utms[i].substring(utms[i].indexOf('=')+1, utms[i].length));
            val = val.replace(/^\(|\)$/g, ''); 
            switch(key)
            {
                case 'utmcsr':
                    source = val;
                    break;
                case 'utmcmd':
                    medium = val;
                    break;
                case 'utmccn':
                    name = val;
                    break;
                case 'utmctr':
                    term = val;
                    break;
                case 'utmcct':
                    content = val;
                    break;
                case 'utmgclid':
                    gclid = val;
                    break;
            }
        }
 
        return {
            'cookie': utmz_array,
            'source': source,
            'medium': medium,
            'name': name,
            'term': term,
            'content': content,
            'gclid': gclid
        };
    };
 
    this.getDomainHash = function() { return (utma() && utma().domainhash) ? utma().domainhash : null };
    this.getUniqueId = function() { return (utma() && utma().uniqueid) ? utma().uniqueid : null };
 
    this.getInitialVisitTime = function() { return (utma() && utma().ftime) ? utma().ftime : null };
    this.getPreviousVisitTime = function() { return (utma() && utma().ltime) ? utma().ltime : null };
    this.getCurrentVisitTime = function() { return (utma() && utma().stime) ? utma().stime : null };
    this.getSessionCounter = function() { return (utma() && utma().sessions) ? utma().sessions : null };
 
    this.getGifRequests = function() { return (utmb() && utmb().gifrequest) ? utmb().gifrequest : null }; 
 
    this.getUserDefinedValue = function () { return (utmv() && utmv().value) ? decodeURIComponent(utmv().value) : null };
 
    this.getSource = function () { return (utmz() && utmz().source) ? utmz().source : null };
    this.getMedium = function () { return (utmz() && utmz().medium) ? utmz().medium : null };
    this.getCampaign = function () { return (utmz() && utmz().name) ? utmz().name : null };
    this.getTerm = function () { return (utmz() && utmz().term) ? utmz().term : null};
    this.getContent = function () { return (utmz() && utmz().content) ? utmz().content : null };
    this.getGclid = function () { return (utmz() && utmz().gclid) ? utmz().gclid : null };
}

function fileLimit(input) {

    var limit = document.getElementById('_crmi_limit');
    var filecount = parseInt(document.getElementById('_crmi_count').value) + 1;
    if (limit.value == 0)
        return;
        
    if (input == null && filecount > 0)
        input = document.getElementById("datafile" + filecount);
    else if (input == null)
        return;

    var broswer = navigator.appName;

    var html5 = false;

    if (html5 && input.files.length > limit.value) {
        alert('You are limited to uploading a maximum of ' + limit.value + ' files. Please make new selections.');
        input.value = null;
        return;
    }
    else if (!html5){
        if (filecount >= parseInt(limit.value))
            return;

        var latch = document.getElementById('_crmi_next');
        var br = document.createElement("br");
        latch.appendChild(br);
        latch.appendChild(br);
        var uploader = document.createElement('input');
        uploader.id = 'datafile' + filecount;
        uploader.name = uploader.id;
        uploader.type = 'file';
        uploader.onchange = fileLimit;
        latch.appendChild(uploader)
        document.getElementById('_crmi_count').value = filecount;
    }

}