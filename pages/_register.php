<?php
/*
 * Template Name: Register
 */
get_header(); ?>

<div class="row content-area">

	<div id="content" class="columns-12 site-content" role="main">
		<div id="content-container">

		<div id="register-header">
			<h1>
				<?php the_field('page_title'); ?>
			</h1>
		</div>
		
		<div id="registerpage-content" class="archer-book">
			<div id="form-container">
				<div class="intro-text"><?php the_field('intro_text'); ?></div>
				<div id="dScript"><script type="text/javascript" language="javascript" src="http://web2crm.crminnovation.com/ValidationChecks.js" ></script></div>
    			<div id="alertDiv"></div>
   				<form method="post" action="http://web2crm.crminnovation.com/FormSubmit/FormSubmit.aspx" id="form1" class="mainForm" onsubmit="cvs();">
					<div class="aspNetHidden"></div>
 					<table class="containerTable">
	       				<tr>
	           				<td width="505" class="containerTd">
	               				<table class="formContainerTable" style="padding: 0px 0px 0px 0px;">
	               					<tr>
		                        		<td class="l"><div></div></td>
		                        		<td class="formContainerTd">
		                            		<table id="formTable" class="formTable">
												<tr></tr>
												<tr>
													<td class="form-label" >First Name * </td>
													<td class="control"><input name="firstname" type="text" maxlength="50" id="firstname" class="form-input" style="width:200px;" /></td>
												</tr>
												<tr>
													<td class="form-label" >Last Name * </td>
													<td class="control"><input name="lastname" type="text" maxlength="50" id="lastname" class="form-input" style="width:200px;" /></td>
												</tr>
												<tr>
													<td class="form-label" >Email Address * </td>
													<td class="control"><input name="emailaddress1" type="text" maxlength="100" id="emailaddress1" class="form-input" onchange="checkEmail(this);" style="width:200px;" /></td>
												</tr>
												<tr>
													<td class="form-label" >Phone * </td>
													<td class="control"><input name="telephone2" type="text" maxlength="50" id="telephone2" class="form-input" style="width:200px;" /></td>
												</tr>
												<tr>
													<td class="form-label" >Postal Code</td>
													<td class="control"><input name="address1_postalcode" type="text" maxlength="20" id="address1_postalcode" class="form-input" style="width:200px;" /></td>
												</tr>
												<tr>
													<td class="form-label" >Comments</td>
													<td class="control">
														<textarea name="new_leadcomments" rows="2" cols="20" id="new_leadcomments" onKeyPress="return ( this.value.length < 10000 );" onPaste="return (( this.value.length + window.clipboardData.getData('Text').length) < 10000);" class="form-input" style="height:100px;width:200px;"></textarea>
													</td>
												</tr>
												<tr>
													<td class="form-label">
														How Did you <br />
				 										Hear About Us *
				 									</td>
				 									<td class="control">
				 										<select name="new_howdidyouhearaboutus" id="new_howdidyouhearaboutus" style="width:200px;">
															<option selected="selected" value="">Select...</option>
															<option value="Vancouver Sun">Vancouver Sun</option>
															<option value=">Province">Province</option>
															<option value="New Home Buyers Guide">New Home Buyers Guide</option>
															<option value="Real Estate Weekly">Real Estate Weekly</option>
															<option value="Burnaby Newsleader">Burnaby Newsleader</option>
															<option value="Burnaby Now">Burnaby Now</option>
															<option value="Vancouver Courier">Vancouver Courier</option>
															<option value="Chinese Condo Guide">Chinese Condo Guide</option>
															<option value="Ming Pao">Ming Pao</option>
															<option value="Sing Tao">Sing Tao</option>
															<option value="Online">Online</option>
															<option value="Signage/Walk By">Signage/Walk By</option>
															<option value="Friend/Family">Friend/Family</option>
															<option value="Realtor">Realtor</option>
															<option value="Email">Email</option>
															<option value="Other">Other</option>

														</select>
													</td>
												</tr>
												<tr>
													<td class="form-label" >Are you a Realtor?</td>
													<td class="control">
														<div class="radioGroup">
															<input id="new_isarealtor_t" type="radio" name="new_isarealtor" value="True" /><form-label for="new_isarealtor_t">Yes</form-label><input id="new_isarealtor_f" type="radio" name="new_isarealtor" value="False" /><form-label for="new_isarealtor_f">No</form-label>
														</div>
													</td>
												</tr>
												<tr>
													<td class="form-label">Brokerage Firm</td>
													<td class="control"><input name="new_brokeragefirm" type="text" maxlength="500" id="new_brokeragefirm" class="form-input" style="width:200px;" /></td>
												</tr>
												<tr class="security-code">
													<td>Security code * : 
														<span id="Web2CRMfieldOne"></span>
														<span style="padding:0 4px 0 4px;">+</span>
														<span id="Web2CRMfieldTwo"></span>
														<span style="padding:0 4px 0 4px;">=</span>
													</td>
													<td class="control"><input name="Web2CRMAnswer" type="text" id="Web2CRMAnswer" class="form-input" /></td>
												</tr>
												<tr>
													<td colspan="2">
												        <div class="bottomButton leftButton">
															<div id="submit-btn">
																<button value="submit" name="ctl06" value="Submit" onclick="if (doClick()) { var go = DoRequiredChecks(); if(go) { var working = document.getElementById('_crmi_working'); working.style.display='block'; return true; } else counter = 0; } return false;" >Submit</button>
															</div>
														</div>
														<!-- <div id="_crmi_working" style="display:none;min-height:100%;width:100%;position:absolute;top:0;left:0;background-color:white;">
															<div style="height:50px;width:50px;position:relative;margin:20% auto auto auto;">
																<img src="http://web2crm.crminnovation.com/loading.gif" height="50" width="50" />
																<span style="text-decoration:blink;">Working...</span>
															</div>
														</div> -->
													</td>
												</tr>
											</table>
		                        		</td>
	                        			<td class="r"><div></div></td>
									</tr>
									<tr>
										<td height="18" class="bl"></td>
										<td class="b"></td>
										<td class="br"></td>
									</tr>
				                </table>
				            </td>
				        </tr>
				    </table>
					<div id="TARGET__" style="display:none; width:100%;">
						<form-label for="crmi_employee_target" >Business Email</form-label>
						<input name="crmi_employee_target" type="text" id="crmi_employee_target" />
					</div>
					<input type="hidden" name="formID" id="formID" value="aff6f7f0-0581-43a5-be93-9255a0108793" />
					<input type="hidden" name="_thankyou_" id="_thankyou_" value="<?php the_field('thank_you_page','options') ?>" />
				</form>
			    <div id="captchaDiv">
			    	<script type="text/javascript" language="javascript">
			    		generateCaptcha(document.getElementById('Web2CRMfieldOne'), document.getElementById('Web2CRMfieldTwo'));</script>
			    		<script type="text/javascript" language="javascript">
				    		var newCss = document.createElement("link"); 
				    		newCss.setAttribute("rel", "stylesheet"); 
				    		newCss.setAttribute("type", "text/css"); 
				    		newCss.setAttribute("href", "form.css"); 
				    		document.getElementsByTagName('head')[0].appendChild(newCss);
			    		</script>
			    		<script type="text/javascript" language="javascript">
				    		function DoRequiredChecks() {
				    			var isValid = true;
				    			if (IsRequired(document.getElementById('firstname')) == false) {
				    				isValid = false;
				    			}if (IsRequired(document.getElementById('lastname')) == false) {
				    				isValid = false;
				    			}if (IsRequired(document.getElementById('emailaddress1')) == false) {
				    				isValid = false;
				    			}if (IsRequired(document.getElementById('telephone2')) == false) {
				    				isValid = false;
				    			}if (IsRequired(document.getElementById('new_howdidyouhearaboutus')) == false) {
				    				isValid = false;
				    			}if(checkCaptcha(document.getElementById('Web2CRMfieldOne'), document.getElementById('Web2CRMfieldTwo'), document.getElementById('Web2CRMAnswer')) == false ){
				    				isValid = false;
				    			}				    			
				    			return isValid;
				    		}
			    		</script>

			    	</div>
				</div>
			</div>
		</div>
	</div>
</div>
		

	</section><!-- #main -->

	<footer id="site-footer" role="contentinfo">
		<div class="footer-wrap">

			<div class="row">
				<div class="columns-3">							
					<?php $footer_logo = get_field('footer_logo','options'); ?>
					<a href="<?php the_field('footer_logo_link','options'); ?>">
						<img src="<?php echo $footer_logo['url']; ?>" alt="Footer Logo">
					</a>
				</div>
				<div class="columns-9">					
					<small><?php the_field('disclaimer'); ?></small>
				</div>
			</div>
				
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>
</body>
</html>