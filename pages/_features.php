<?php
/*
 * Template Name: Features & Finishes
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content features-finishes" role="main">
			<div class="page-wrap">
			<?php while ( have_posts() ) : the_post(); ?>
				

				<?php if(get_field('has_banner') == TRUE ) { get_template_part( 'templates/content', 'banner' ); } ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('page-content'); ?>>

					<div class="entry-content">
					
						<?php the_field('content'); ?>

						<?php $button_text = get_field('button_text');

						$button_link = get_field('button_file_link');

						if( !$button_link){
							$button_link = get_field('button_link');
						}

						 ?>

						<?php if( $button_text && $button_link ): ?>

							<a href="<?php echo $button_link; ?>" target="_blank" class="page-button"><?php echo $button_text; ?></a>
						<?php endif; ?>

					</div><!-- .entry-content -->


					<div class="scroll-down">
						<a href="#finish-callouts" class=""></a>
					</div>

				</article><!-- #post-## -->
				
				<?php if(get_field('callouts')): ?>
					<div id="finish-callouts">
						<div class="grid-wrap">
							<ul class="block-grid-3">
								<?php while(have_rows('callouts')): the_row(); ?>
									<li>
										<h3><?php the_sub_field('title'); ?></h3>
										<?php the_sub_field('content'); ?>										
									</li>
								<?php endwhile; ?>
							</ul>
						</div>
						<ul class="block-grid-3 image-block">
							<?php while(have_rows('callouts')): the_row(); ?>
								<?php if(get_sub_field('image')): ?>
									<li>
										<?php $image = get_sub_field('image'); ?>
										<img src="<?php echo $image['sizes']['features-grid']; ?>" alt="<?php the_sub_field('title'); ?>">
									</li>
								<?php endif; ?>
							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>

			<?php endwhile; // end of the loop. ?>

			<?php if(get_field('bottom_image')): ?>

				<div class="bottom-image">
					<img src="<?php $image = get_field('bottom_image'); echo $image['url']; ?>">
				</div>
			<?php endif; ?>
			</div>
		</div><!-- #content -->

	</div>
		
<?php get_footer(); ?>
