<?php
/*
 * Template Name: Contact Page
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				
				<div id="contact-details">

					<?php if(get_field('button_text')): ?>
						<div class="page-button-wrap">
							<?php if(get_field('button_link')): ?>
								<a href="<?php the_field('button_link'); ?>" target="_blank" class="page-button"><?php the_field('button_text'); ?></a>
							<?php else: ?>
								<a class="page-button"><?php the_field('button_text'); ?></a>				
						<?php endif; ?>
						</div>
					<?php endif; ?>


					<?php while(have_rows('contact_info')): the_row();?>
						<div class="info-block">
							<p class="label"><?php the_sub_field('label'); ?></p>
							<?php the_sub_field('info'); ?>
						</div>
					<?php endwhile; ?>
					<?php if(get_field('map_button_text') && get_field('map_button_link')): ?>
						<a href="<?php the_field('map_button_link') ?>" target="_blank" class="page-button"><?php the_field('map_button_text') ?></a>
					<?php endif; ?>
				</div>

				<div id="map-box" class="map-box" data-map-marker="<?php bloginfo('template_directory'); ?>/images/icon-map-marker.png">
					<?php // the_field('map_link'); ?>

					<?php 
		 
					$location = get_field('contact_map');
					 
					if( !empty($location) ):
					?>
					<div class="acf-map">
						<div class="marker" id="map-marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
					</div>
					<?php endif; ?>
				</div>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->

		

	</div>
		
<?php get_footer(); ?>