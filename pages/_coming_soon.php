<?php
/*
 * Template Name: Coming Soon
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">
			<div class="page-wrap">
			<?php while ( have_posts() ) : the_post(); ?>
				

				<?php if(get_field('has_banner') == TRUE ) { get_template_part( 'templates/content', 'banner' ); } ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('page-content'); ?>>

					<div class="entry-content">
					
						<?php the_field('content'); ?>

						<?php $button_text = get_field('button_text');
						$button_link = get_field('button_file_link');
						if( !$button_link){
							$button_link = get_field('button_link');
						}

						$fancy_footer = get_field('fancybox_footer_title');
						?>

						<?php if( $button_text && $button_link ): ?>
							<a href="<?php echo $button_link; ?>" class="page-button"><?php echo $button_text; ?></a>
						<?php endif; ?>

					</div><!-- .entry-content -->


				</article><!-- #post-## -->
				
				<div class="bottom-image">
					<img src="<?php $image = get_field('bottom_image'); echo $image['url']; ?>">
				</div>
				
			<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- #content -->
	</div>
		
<?php get_footer(); ?>
