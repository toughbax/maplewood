<?php
/*
 * Template Name: Floor Plans
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">
			<div class="page-wrap">
			<?php while ( have_posts() ) : the_post(); ?>
				

				<?php if(get_field('has_banner') == TRUE ) { get_template_part( 'templates/content', 'banner' ); } ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('page-content'); ?>>

					<div class="entry-content">
					
						<?php the_field('content'); ?>

						<?php $button_text = get_field('button_text');
						$button_link = get_field('button_file_link');
						if( !$button_link){
							$button_link = get_field('button_link');
						}

						$fancy_footer = get_field('fancybox_footer_title');
						?>

						<?php if( $button_text && $button_link ): ?>
							<a href="<?php echo $button_link; ?>" class="page-button"><?php echo $button_text; ?></a>
						<?php endif; ?>

					</div><!-- .entry-content -->


				</article><!-- #post-## -->
					
					<?php if(get_field('bottom_image')): ?>
						<div class="bottom-image">
							<img src="<?php $image = get_field('bottom_image'); echo $image['url']; ?>">
						</div>
					<?php endif; ?>
				<?php if(!get_field('hide_floorplans') && get_field('gallery_rows')): ?>
					<div  class="gallery-block">
						<?php if( have_rows('gallery_rows') ):			
							$row_count = 1;			 
						    while ( have_rows('gallery_rows') ) : the_row(); ?>

						    	<?php if($row_count++ == 1 ): ?>
									<div id="gallery-row-<?php echo $count ?>-1"  class="gallery-row">		
						    	<?php else: ?>
									<div class="gallery-row">
						    	<?php endif; ?>
								
							        <?php if( get_row_layout() == 'single_image' ): ?>
							 			<?php $image = get_sub_field('image'); ?>
							 			<?php $thumb = get_sub_field('thumbnail'); ?>
							 			<a class="fancybox" title="<?php the_sub_field('caption'); ?>" href="<?php echo $image['url']; ?>">
							 				<img src="<?php echo $thumb['sizes']['gallery-single']; ?>" alt="Gallery Image">
							 			</a>
							        <?php elseif( get_row_layout() == 'two_images' ): ?>
										<ul class="block-grid-2">
											<li>											
										 		<?php $image_one = get_sub_field('image_one'); ?>
										 		<?php $thumb_one = get_sub_field('thumbnail_one'); ?>
							 					<a class="fancybox" title="<?php the_sub_field('caption_one'); ?>" href="<?php echo $image_one['url']; ?>">	
										 			<img src="<?php echo $thumb_one['sizes']['gallery-double']; ?>" alt="Gallery Image">
									 			</a>
											</li>
											<li>											
										 		<?php $image_two = get_sub_field('image_two'); ?>
										 		<?php $thumb_two = get_sub_field('thumbnail_two'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_two'); ?>" href="<?php echo $image_two['url']; ?>">	
										 			<img src="<?php echo $thumb_two['sizes']['gallery-double']; ?>" alt="Gallery Image">
									 			</a>
											</li>
										</ul>
							        <?php elseif( get_row_layout() == 'three_images' ): ?>

										<ul class="block-grid-3">
											<li>
										 		<?php $image_one = get_sub_field('image_one'); ?>
										 		<?php $thumb_one = get_sub_field('thumbnail_one'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_one'); ?>" href="<?php echo $image_one['url']; ?>">												
										 			<img src="<?php echo $thumb_one['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
											<li>
										 		<?php $image_two = get_sub_field('image_two'); ?>
										 		<?php $thumb_two = get_sub_field('thumbnail_two'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_two'); ?>" href="<?php echo $image_two['url']; ?>">												
										 			<img src="<?php echo $thumb_two['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
											<li>
										 		<?php $image_three = get_sub_field('image_three'); ?>
										 		<?php $thumb_three = get_sub_field('thumbnail_three'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_three'); ?>" href="<?php echo $image_three['url']; ?>">												
										 			<img src="<?php echo $thumb_three['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
										</ul>
							        <?php elseif( get_row_layout() == 'text' ): ?>
							 			<p><?php the_sub_field('text'); ?></p>
							        <?php endif; ?>
						 		</div>
						    <?php endwhile;						 
						endif; ?>

						<div class="scroll-down">
							<a href="#floorplans" class=""></a>
						</div>
					</div>
				<?php endif; ?>

				<?php if(!get_field('hide_floorplans')): ?>
					
					<?php if (get_field('floor_plan_intro')): ?>
						<div class="floorplan-intro">
							<article class="page-content">
						
								<?php the_field('floor_plan_intro'); ?>

								<?php $button_text = get_field('floor_plan_intro_button_text');
								$button_link = get_field('floor_plan_intro_button_link');

								?>

								<?php if( $button_text && $button_link ): ?>
									<a href="<?php echo $button_link; ?>" class="page-button"><?php echo $button_text; ?></a>
								<?php endif; ?>

							</article><!-- .entry-content -->
		
						</div>
					<?php endif; ?>
					<div id="floorplans">
						<?php $area_maps = get_terms('area-maps','orderby=id');?>
						<?php $map_count = 0; ?>
						<div id="area-map-controls">
							
							<div class="title">
								<?php the_field("level_select_title"); ?>
							</div>	
							<ul>
								<?php foreach($area_maps as $area_map): ?>
									<li>
										<a href="#" class="<?php if($map_count++ == 0){echo "active "; } ?> level-change" data-map-name="<?php echo $area_map->slug; ?>">
											<?php echo $area_map->name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>

						<div id="area-maps">
							<?php $map_count = 0; ?>
							<?php foreach($area_maps as $area_map): ?>
								<div class="area-map <?php if($map_count++ == 0){echo "active "; } ?> <?php echo $area_map->slug; ?>">

									<?php 
									// 	get current term ID;
									$term_id = $area_map->term_id;
									$taxonomy = $area_map->taxonomy;
									$tax_field = $taxonomy . "_" . $term_id;
									$image = get_field('area_map',$tax_field); 

									$home_types = Array();
									$type_count = 1;
									 
									//Get all Homes related to this area map
									$args = array(
										'post_type' => 'homes', 
										'posts_per_page' => -1,
										'orderby' => 'menu_order',
										'order' => 'ASC',
										'tax_query' => array(
											array(
												'taxonomy' => $taxonomy,
												'field' => 'id',
												'terms' => $term_id,
												'operator' => 'IN'
											)
										)

									); 
									$homes = new WP_Query($args); 
									?>
									<div class="homes">
										<div class="title">Choose a Plan</div>
										<ul class="homes-list">
											<?php while ( $homes->have_posts() ) : $homes->the_post(); ?>

												<!-- Get the placement values from current home-->
												<?php 
												$top_value = get_field('top_value');
												$left_value = get_field('left_value');

												$sold_image = get_field('sold_state');											
												$home_image = get_field('hover_state');

												$class = "";

												if( get_field('sold') == TRUE){
													$background_image = $sold_image;
													$class = " sold ";
												}else{
													$background_image = $home_image;
												}
												

												$height = $home_image['height']/$image['height']*100;
												$width = $home_image['width']/$image['width']*100;

												$floor_plans = get_the_terms( get_the_id(), 'floor-plans' ); 
												$floor_plan;
												$count = 0;
												foreach($floor_plans as $fp){
													if($count++ == 0){
														$floor_plan = $fp;
													}
												}
												$floor_plan = $floor_plan;
												$floor_plan_id = $floor_plan->term_id;



												if(!in_array($floor_plan->name, $home_types)){
													$class .= " new_home_type";
													$home_types[] = $floor_plan->name;

													if($type_count++ % 2 == 0){
														$class .= " even";
													}else{
														$class .= " odd";
													}
												}
												
												?>

												<!-- Single Home. 
												-If the placement values are set, set them as both data attributes as
												 well as left/top CSS Values
												-Set Background image to the hover state of the Home.
												-Set width and Height to match hover state of home
												-->
												<li 
													class="single-home home-<?php echo get_the_id(); ?> <?php echo $class; ?>"
													data-home-id="<?php echo get_the_id(); ?>"
													style="<?php echo ($top_value ? "top:" . $top_value . "%;":""). ($left_value ? "left:" . $left_value . "%;":"");?>
													background:url('<?php echo $background_image['url']; ?>') no-repeat bottom center;
													height: <?php echo $height; ?>%; width: <?php echo $width ?>%;
													background-size: 100% auto;
													">
													<?php if( !get_field('sold') == TRUE ): ?>
														<a href="#fancy-box-<?php the_ID(); ?>" class="fancy-floorplan"></a>
													<?php endif; ?>
													<div class="hover" style="bottom:<?php echo $home_image['height']; ?>px;">
														<h4>PLAN <?php the_field('plan_title',"floor-plans_$floor_plan_id"); ?></h4>
														<p><?php the_field('hover_info',"floor-plans_$floor_plan_id") ?></p>
													</div>

													<div class="mobile-content">
														<a href="#fancy-box-<?php the_ID(); ?>" class="mobile-fancy-floorplan">
															<h3>Plan <?php the_field('plan_title',"floor-plans_$floor_plan_id"); ?></h3>
															<p><?php the_field('mobile_info',"floor-plans_$floor_plan_id") ?></p>
														</a>
													</div>

													<!-- MODAL (FANCYBOX) POPUP -->
													<article id="fancy-box-<?php the_ID(); ?>" style="display:none" <?php post_class(''); ?>>
														<div class="top-panel">
															<div class="general-info">
																<h1><?php the_field('plan_title',"floor-plans_$floor_plan_id"); ?></h1>
																<p><?php the_field('pop_up_info',"floor-plans_$floor_plan_id") ?></p>
															</div>
															<div class="location-info">
																<p><?php the_field('direction_info',"floor-plans_$floor_plan_id"); ?></p>
															</div>
															<a href="<?php the_field('floor_plan_pdf',"floor-plans_$floor_plan_id"); ?>" class="download-link">Download Plan</a>
														</div>
														<div class="floor-plan-images">
															<?php $image_array = get_field('floor_plan_images',"floor-plans_$floor_plan_id");
															$image_count = count($image_array);
															 ?>
															<ul class="block-grid-<?php echo $image_count; ?>">
																<?php foreach($image_array as $fp_image): ?>
																	<li>
																		<img src="<?php echo $fp_image['image']['url']; ?>" alt="Floor Plan">
																	</li>
																<?php endforeach; ?>
															</ul>
														</div>
														<div class="bottom-panel">
															<div class="panel-compass">
																<?php $compass = get_field('map_compass','options'); ?>
																<img src="<?php echo $compass['url']; ?>">
															</div>
															<div class="bottom-area-maps">
																<?php $image_array = get_field('area_maps',"floor-plans_$floor_plan_id");
																$image_count = count($image_array);
																 ?>
																<ul class="block-grid-<?php echo $image_count; ?>">
																	<?php foreach($image_array as $fp_image): ?>
																		<li>
																			<img src="<?php echo $fp_image['image']['url']; ?>" alt="Floor Plan">
																		</li>
																	<?php endforeach; ?>
																</ul>

																<p class="fancy-footer"><?php echo $fancy_footer; ?></p>
															</div>
														</div>											
													</article>
												</li>

											<?php endwhile;?>

										</ul>
									</div>

									<?php if($term_id == 9): ?>
										<div class="split-home block-1"></div>
										<div class="split-home block-2"></div>
										<div class="split-home block-3"></div>
										<div class="split-home block-4"></div>
									<?php endif; ?>
									<img src="<?php echo $image['url']; ?>" id="fs_image_map" style="max-width:100%;" class="image-map" alt="Location Map">
									
								</div>
							<?php endforeach; ?>
						</div>	

						<script>
						jQuery(document).ready(function($){
							$('#area-map-controls').on('click','.level-change',function(e){
								e.preventDefault();
								var map_name = $(this).attr('data-map-name');
								$('#area-map-controls').find('a').each(function(){
									$(this).removeClass('active');
								});
								$(this).addClass('active');
								$('#area-maps').find('.area-map').each(function(){
									if($(this).hasClass(map_name)){
										$(this).addClass('active');
									}else{
										$(this).removeClass('active');
									}
								});
							});

							$('.split-home.block-1').on('hover',function(){
								console.log('text');
								$('.home-223').toggleClass('active');						
							});
							$('.split-home.block-1').on('click',function(){
								console.log('text');
								$('.home-223 .fancy-floorplan').trigger('click');			
							});


							$('.split-home.block-2').on('hover',function(){
								console.log('text');
								$('.home-306').toggleClass('active');						
							});
							$('.split-home.block-2').on('click',function(){
								console.log('text');
								$('.home-306 .fancy-floorplan').trigger('click');			
							});


							$('.split-home.block-3').on('hover',function(){
								console.log('text');
								$('.home-309').toggleClass('active');						
							});
							$('.split-home.block-3').on('click',function(){
								console.log('text');
								$('.home-309 .fancy-floorplan').trigger('click');			
							});


							$('.split-home.block-4').on('hover',function(){
								console.log('text');
								$('.home-312').toggleClass('active');						
							});
							$('.split-home.block-4').on('click',function(){
								console.log('text');
								$('.home-312 .fancy-floorplan').trigger('click');			
							});

						});
						</script>

					</div>
				<?php endif; ?>
			<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- #content -->
	</div>
		
<?php get_footer(); ?>
