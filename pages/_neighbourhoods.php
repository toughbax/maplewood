<?php
/*
 * Template Name: Neighbourhoods
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">
			<div class="page-wrap">
			<?php while ( have_posts() ) : the_post(); ?>
				

				<?php if(get_field('has_banner') == TRUE ) { get_template_part( 'templates/content', 'banner' ); } ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('page-content'); ?>>


					<h3><?php the_field('content_title'); ?></h3>

					<?php $image = get_field('content_display_image'); ?>
					<?php $popup = get_field('content_popup_image'); ?>
					<?php if( $image ): ?>
						<div class="neighbourhood-popup">
							<?php if($popup): ?>
								<a href="<?php echo $popup['sizes']['community-map']; ?>" class="fancybox ">
									<img src="<?php echo $image['sizes']['community-map']; ?>">
								</a>
							<?php else: ?>
								<img src="<?php echo $image['sizes']['community-map']; ?>">
							<?php endif; ?>
						</div>

					<?php endif; ?>

					<div class="entry-content">
					
						<?php the_field('content'); ?>

						<?php $button_text = get_field('button_text');

						$button_link = get_field('button_file_link');

						if( !$button_link){
							$button_link = get_field('button_link');
						}

						?>

						<?php if( $button_text && $button_link ): ?>

							<a href="<?php echo $button_link; ?>" class="page-button"><?php echo $button_text; ?></a>
						<?php endif; ?>

					</div><!-- .entry-content -->


					<div class="scroll-down">
						<a href="#finish-callouts" class=""></a>
					</div>

				</article><!-- #post-## -->
				
				<?php if(get_field('callouts')): ?>
					<div id="finish-callouts">
						<div class="grid-wrap">
							<ul class="block-grid-3">
								<?php while(have_rows('callouts')): the_row(); ?>
									<li>
										<h3><?php the_sub_field('title'); ?></h3>
										<?php the_sub_field('content'); ?>
									</li>
								<?php endwhile; ?>
							</ul>
						</div>
						<ul class="block-grid-3 image-block">
							<?php while(have_rows('callouts')): the_row(); ?>
								<li>
									<?php $image = get_sub_field('image'); ?>
									<img src="<?php echo $image['sizes']['features-grid']; ?>" alt="<?php the_sub_field('title'); ?>">
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>


				<?php if( get_field('gallery_rows')): ?>
					<div  class="gallery-block">
						<?php if( have_rows('gallery_rows') ):			
							$row_count = 1;			 
						    while ( have_rows('gallery_rows') ) : the_row(); ?>

						    	<?php if($row_count++ == 1 ): ?>
									<div id="gallery-row-<?php echo $count ?>-1"  class="gallery-row">		
						    	<?php else: ?>
									<div class="gallery-row">
						    	<?php endif; ?>
								
							        <?php if( get_row_layout() == 'single_image' ): ?>
							 			<?php $image = get_sub_field('image'); ?>
							 			<?php $thumb = get_sub_field('thumbnail'); ?>
							 			<a class="fancybox" title="<?php the_sub_field('caption'); ?>" href="<?php echo $image['url']; ?>">
							 				<img src="<?php echo $thumb['sizes']['gallery-single']; ?>" alt="Gallery Image">
							 			</a>
							        <?php elseif( get_row_layout() == 'two_images' ): ?>
										<ul class="block-grid-2">
											<li>											
										 		<?php $image_one = get_sub_field('image_one'); ?>
										 		<?php $thumb_one = get_sub_field('thumbnail_one'); ?>
							 					<a class="fancybox" title="<?php the_sub_field('caption_one'); ?>" href="<?php echo $image_one['url']; ?>">	
										 			<img src="<?php echo $thumb_one['sizes']['gallery-double']; ?>" alt="Gallery Image">
									 			</a>
											</li>
											<li>											
										 		<?php $image_two = get_sub_field('image_two'); ?>
										 		<?php $thumb_two = get_sub_field('thumbnail_two'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_two'); ?>" href="<?php echo $image_two['url']; ?>">	
										 			<img src="<?php echo $thumb_two['sizes']['gallery-double']; ?>" alt="Gallery Image">
									 			</a>
											</li>
										</ul>
							        <?php elseif( get_row_layout() == 'three_images' ): ?>

										<ul class="block-grid-3">
											<li>
										 		<?php $image_one = get_sub_field('image_one'); ?>
										 		<?php $thumb_one = get_sub_field('thumbnail_one'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_one'); ?>" href="<?php echo $image_one['url']; ?>">												
										 			<img src="<?php echo $thumb_one['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
											<li>
										 		<?php $image_two = get_sub_field('image_two'); ?>
										 		<?php $thumb_two = get_sub_field('thumbnail_two'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_two'); ?>" href="<?php echo $image_two['url']; ?>">												
										 			<img src="<?php echo $thumb_two['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
											<li>
										 		<?php $image_three = get_sub_field('image_three'); ?>
										 		<?php $thumb_three = get_sub_field('thumbnail_three'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_three'); ?>" href="<?php echo $image_three['url']; ?>">												
										 			<img src="<?php echo $thumb_three['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
										</ul>
							        <?php elseif( get_row_layout() == 'text' ): ?>
							 			<p><?php the_sub_field('text'); ?></p>
							        <?php endif; ?>
						 		</div>
						    <?php endwhile;						 
						endif; ?>

					</div>
				<?php endif; ?>


				<div id="map-intro">
					<?php 
					the_field('intro_text');

					$button_link = get_field('map_button_link');
					$button_text = get_field('map_button_text');
					if($button_text && $button_link ): ?>
						<a href="<?php echo $button_link; ?>" class="button"><?php echo $button_text; ?></a>
					<?php endif; ?>
					<div class="scroll-down">
						<a href="#neighbourhood-map" class=""></a>
					</div>
				</div>

				<div id="neighbourhood-map">
					<?php $thurston_location = get_field('street_location'); ?>
					<div 
						id="map" 
						data-map-marker="<?php bloginfo('template_directory'); ?>/images/icon-map-marker.png" 
						data-thurston-lat="<?php echo $thurston_location['lat'] ?>"
						data-thurston-lng="<?php echo $thurston_location['lng'] ?>"
						>
						<!-- GET ARRAY OF LOCATION DATA  -->
						<?php $locations_array = fs_get_neighbourhood_locations() ?>
						<script>
							// Create a JS array to hold location data
							var sites = [];
						</script>

						<?php foreach( $locations_array as $location_type ): ?>
							<?php foreach($location_type['locations'] as $location): ?>
								<script>
									// loop through each location and add the info to the array
									// this array is used in neighbourhoods.js
									sites.push([
										// Location Name
										"<?php echo $location['name']; ?>",
										//Location Lat
										"<?php echo $location['location']['lat']; ?>",
										//Locaiton Long
										"<?php echo $location['location']['lng']; ?>",
										//Locaiton Z Index
										5,
										//Locaiton HTML (for map popup);
										"<div class='info-box'><h4><?php echo $location['name']; ?></h4></div>",
										//location type
										"<?php echo $location_type['term']->slug; ?>",
										//location type marker
										"<?php the_field('color', 'location-types_' . $location_type['term']->term_id); ?>",
										//location ID
										"<?php echo $location['count']; ?>"
									]);
								</script>
							<?php endforeach; ?>
						<?php endforeach; ?>
					</div>

					<div id="map-filters">


						<div class="tabs">
							<ul class="slider-thumbs">
								<?php foreach( $locations_array as $location_type ): ?>
									<li class="catFilter" data-filter-color="<?php the_field('color', 'location-types_'.$location_type['term']->term_id ); ?>" data-filter-name="<?php echo $location_type['term']->slug; ?>"><?php echo $location_type['term']->name; ?></li> 
								<?php endforeach; ?>
							</ul>
							<div class="flexslider tab-slider">
								<ul class="slides">
								<?php foreach( $locations_array as $location_type ): ?>
									<li class="" data-filter-name="<?php echo $location_type['term']->slug; ?>"> 
										<ul class="block-grid-4">
										<?php foreach($location_type['locations'] as $location): ?>
											<li>
												<a href="#map" class="single-location" data-location-count="<?php echo $location['count']; ?>"><?php echo $location['name']; ?></a>
											</li>
										<?php endforeach; ?>
										</ul>
									</li>
								<?php endforeach; ?>
											
								</ul>
							</div>
						</div>

						<div class="accordions">
							<ul class="accordion">
								<?php $accordion_count = 0; ?>
								<?php foreach( $locations_array as $location_type ): ?>
									<li class="<?php echo ($accordion_count++ == 0 ) ? "flex-active" : "" ; ?>"> 
										<div class="catFilter title" data-filter-color="<?php the_field('color', 'location-types_'.$location_type['term']->term_id ); ?>"  data-filter-name="<?php echo $location_type['term']->slug; ?>">
											<h5><?php echo $location_type['term']->name; ?></h5>
										</div>
										<div class="content">											
											<ul class="block-grid-2">
											<?php foreach($location_type['locations'] as $location): ?>
												<li>
													<a href="#map" class="single-location" data-location-count="<?php echo $location['count']; ?>"><?php echo $location['name']; ?></a>
												</li>
											<?php endforeach; ?>
											</ul>
										</div>
									</li>
								<?php endforeach; ?>
						</div>
					</div>
				</div>

				<div id="bottom-callouts">
					<?php while(have_rows('bottom_content')): the_row(); ?>

						<div class="bottom-row">
							<div class="row">
								<div class="columns-7 image-column">
									<?php $image = get_sub_field('image'); ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('header') ?>">
								</div>
								<div class="columns-5 text-column">
									<h5><?php the_sub_field('header'); ?></h5>
									<h3><?php the_sub_field('title'); ?></h3>
									<?php the_sub_field('text'); ?>
								</div>
							</div>
						</div>

					<?php endwhile; ?>
				</div>

			<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- #content -->
	</div>
		
<?php get_footer(); ?>
