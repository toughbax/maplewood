<?php
/*
 * Template Name: Gallery
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php $block_count = count(get_field('gallery_blocks')); $count = 1; ?>
				
				<?php while(have_rows('gallery_blocks')): the_row(); ?>
					
					<article id="gallery-block-<?php echo $count; ?>" class="gallery-block">
						
						<div class="introduction">
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php the_sub_field('intro') ?></p>

							<?php 
							$button_text = get_sub_field('intro_button_text');
							$button_link = get_sub_field('intro_button_link');
							if(!$button_link){
								$button_link = "#gallery-row-".$count ."-1";
							}
							?>

							<?php if( $button_text && $button_link ): ?>
								<a href="<?php echo $button_link; ?>" class="page-button"><?php echo $button_text; ?></a>
							<?php endif; ?>
						</div>

						<?php if( have_rows('rows') ):			
							$row_count = 1;			 
						    while ( have_rows('rows') ) : the_row(); ?>

						    	<?php if($row_count++ == 1 ): ?>
									<div id="gallery-row-<?php echo $count ?>-1"  class="gallery-row">		
						    	<?php else: ?>
									<div class="gallery-row">
						    	<?php endif; ?>
								
							        <?php if( get_row_layout() == 'single_image' ): ?>
							 			<?php $image = get_sub_field('image'); ?>
							 			<?php $thumb = get_sub_field('thumbnail'); ?>
							 			<a class="fancybox" title="<?php the_sub_field('caption'); ?>" href="<?php echo $image['url']; ?>">
							 				<img src="<?php echo $thumb['sizes']['gallery-single']; ?>" alt="Gallery Image">
							 			</a>
							        <?php elseif( get_row_layout() == 'two_images' ): ?>
										<ul class="block-grid-2">
											<li>											
										 		<?php $image_one = get_sub_field('image_one'); ?>
										 		<?php $thumb_one = get_sub_field('thumbnail_one'); ?>
							 					<a class="fancybox" title="<?php the_sub_field('caption_one'); ?>" href="<?php echo $image_one['url']; ?>">	
										 			<img src="<?php echo $thumb_one['sizes']['gallery-double']; ?>" alt="Gallery Image">
									 			</a>
											</li>
											<li>											
										 		<?php $image_two = get_sub_field('image_two'); ?>
										 		<?php $thumb_two = get_sub_field('thumbnail_two'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_two'); ?>" href="<?php echo $image_two['url']; ?>">	
										 			<img src="<?php echo $thumb_two['sizes']['gallery-double']; ?>" alt="Gallery Image">
									 			</a>
											</li>
										</ul>
							        <?php elseif( get_row_layout() == 'three_images' ): ?>

										<ul class="block-grid-3">
											<li>
										 		<?php $image_one = get_sub_field('image_one'); ?>
										 		<?php $thumb_one = get_sub_field('thumbnail_one'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_one'); ?>" href="<?php echo $image_one['url']; ?>">												
										 			<img src="<?php echo $thumb_one['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
											<li>
										 		<?php $image_two = get_sub_field('image_two'); ?>
										 		<?php $thumb_two = get_sub_field('thumbnail_two'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_two'); ?>" href="<?php echo $image_two['url']; ?>">												
										 			<img src="<?php echo $thumb_two['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
											<li>
										 		<?php $image_three = get_sub_field('image_three'); ?>
										 		<?php $thumb_three = get_sub_field('thumbnail_three'); ?>
												<a class="fancybox" title="<?php the_sub_field('caption_three'); ?>" href="<?php echo $image_three['url']; ?>">												
										 			<img src="<?php echo $thumb_three['sizes']['gallery-triple']; ?>" alt="Gallery Image">
										 		</a>
											</li>
										</ul>
							        <?php elseif( get_row_layout() == 'text' ): ?>
							 			<p><?php the_sub_field('text'); ?></p>
							        <?php endif; ?>
						 		</div>
						    <?php endwhile;						 
						endif; ?>

						<?php if($count++ < $block_count): ?>
							

							<div class="scroll-down">
								<a href="#gallery-block-<?php echo $count; ?>" class=""></a>
							</div>

						<?php endif; ?>

					</article>

				<?php endwhile; ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->

	</div>
		
<?php get_footer(); ?>
