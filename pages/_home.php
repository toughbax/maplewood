<?php
/*
 *  Template Name: Home Page
 * 
 *	Front page is typically a series of template parts that will differ
 *	depending the design. 
 *
 *
 */

get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'templates/content', 'page' ); ?>

				<?php //get_template_part( 'templates/content', 'slider' );?>

			<?php endwhile; // end of the loop. ?>

			<div id="home-slideshow">
				
				<div class="flexslider slider">
					
					<ul class="slides">
					
					<?php while(has_sub_field('slideshow')): ?>			

						<li>
							<!-- <div class="row"> -->
								<!-- <div class="columns-4 text-wrap">

									<?php the_sub_field('text'); ?>

								</div>

								<div class="columns-8 image-wrap">
									<?php $image = get_sub_field('image'); ?>
									<?php if(get_sub_field('link')): ?>
										<a href="<?php the_sub_field('link'); ?>">	
											<img src="<?php echo $image['sizes']['home-slideshow'] ?>" alt="Slideshow Image">
										</a>
									<?php else: ?>
										<img src="<?php echo $image['sizes']['home-slideshow'] ?>" alt="Slideshow Image">	
									<?php endif; ?>	
								</div> -->
								<div class="image-wraps">
									<?php $image = get_sub_field('image'); ?>
									<?php if(get_sub_field('link')): ?>
										<a href="<?php the_sub_field('link'); ?>">	
											<img src="<?php echo $image['url'] ?>" alt="Slideshow Image">
										</a>
									<?php else: ?>
										<img src="<?php echo $image['url'] ?>" alt="Slideshow Image">	
									<?php endif; ?>	
								</div>
							<!-- </div>	 -->
						</li>

					<?php endwhile;  ?>
					
					</ul>
					
				</div>	

			</div>

			<div id="home-intro">
				<?php 
				the_field('introduction_text');

				$button_link = get_field('introduction_button_link');
				$button_text = get_field('introduction_button_text');
				if($button_text && $button_link ): ?>
					<a href="<?php echo $button_link; ?>" class="button"><?php echo $button_text; ?></a>
				<?php endif; ?>
				<div class="scroll-down">
					<a href="#home-featured" class=""></a>
				</div>
			</div>
			
			<?php $featured_image = get_field('featured_image'); ?>
			<div id="home-featured" style="
				background-image: url('<?php echo $featured_image['url']; ?>');
				background-repeat: no-repeat;
				background-position: center;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover; 
				">
				<img src="<?php echo $featured_image['url']; ?>" class="mobile-image" alt="Featured Image">
				<div class="text-wrap">
					<?php the_field('featured_text'); ?>
					<?php while(have_rows('featured_buttons')): the_row(); ?>

						<a href="<?php the_sub_field('button_link'); ?>" class="button"><?php the_sub_field('button_text'); ?></a>

					<?php endwhile; ?>
				</div>
			</div>

			<div id="home-callout">
				<div class="row">
					<div class="columns-6 text text-column">
						<?php the_field('callout_text'); ?>
						<a href="<?php the_field('callout_button_link'); ?>" class="button"><?php the_field('callout_button_text'); ?></a>
					</div>
					<div class="columns-6 image-column">
						<?php $featured_image = get_field('callout_image'); ?>
						<img src="<?php echo $featured_image['url']; ?>" alt="Callout Image">
					</div>
				</div>
			</div>

		</div><!-- #content -->

	</div>
		
<?php get_footer(); ?>
