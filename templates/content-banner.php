<?php
/*
 * The template used for displaying page content in page.php
 */
?>
<?php $hero_image = get_field("banner_image"); ?>
<div id="hero-banner" style="
	background-image: url('<?php echo $hero_image['url']; ?>');
	background-repeat: no-repeat;
	background-position: center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover; 
	">

		<div class="text-container text-center">
													
			<h1>
				<?php the_field('banner_text'); ?>
			</h1>

		</div>


	<div class="scroll-down">
		<a href="#post-<?php the_ID(); ?>" class=""></a>
	</div>
</div>
