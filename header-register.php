<?php
/**
 * The Header for our theme.
 * Displays all of the <head> section and everything up till <div id="main">
 */
?>


<!DOCTYPE html>
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 8]> 				 <html class="no-js lt-ie9" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 9]> 				 <html class="no-js lt-ie10" <?php language_attributes(); ?> > <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link rel="stylesheet" type="text/css" href="//cloud.typography.com/727240/676984/css/fonts.css" />

<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>

<?php wp_head(); ?>
</head>
<?php $background_image = get_field('site_background_image','options'); ?>
<?php $background_color = get_field('site_background_color','options'); ?>
<body <?php body_class(); ?> style="
	background: <?php echo $background_color; ?> url(<?php echo $background_image; ?>) no-repeat center bottom; 
	background-size: 100% auto;"
>

	<header class="top-banner">
		
		<p class="top-banner-tag">
			<?php the_field('top_banner_tagline','options'); ?>
		</p>
		<p class="neighbourhoods-link">
			<?php the_field('top_banner_button_text','options'); ?>
		</p>

	</header>

	<div id="top-banner-flydown">
		<div class="text-intro">
			<p><?php the_field('drop_down_text','options'); ?></p>
			<a href="<?php the_field('drop_down_link_location','options') ?>"><?php the_field('drop_down_link_text','options') ?></a>
		</div>
		<div class="callouts">
			<ul>
			<?php while(have_rows('drop_down_blocks','options')): the_row(); ?>
				
				<li>
					<?php 
					$image = get_sub_field('icon'); 
					$link = get_sub_field('link');
					?>

					<?php if($link): ?>
						<a href="<?php echo $link; ?>" target="_blank">
							<img src="<?php echo $image['url']; ?>" alt="Neighbourhood">
						</a>
					<?php else: ?>
						<img src="<?php echo $image['url']; ?>" alt="Neighbourhood">
					<?php endif; ?>
				</li>

			<?php endwhile; ?>
			</ul>
		</div>
	</div>


	<header id="masthead" class="site-header panel" role="banner">

		<div class="main-banner">
		
			<div class="row">
		
				<div class="columns-2 site-branding">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="site logo" />
					</a>
				</div>
								
				<nav id="site-navigation" class="columns-10 navigation-main" role="navigation">

					<a href="<?php the_field('register_button_link','options'); ?>" class="register-button"><?php the_field('register_button_text','options'); ?></a>

					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'items_wrap' => '<ul id="main-nav" class="nav menu dropmenu">%3$s</ul>', 'walker' => new Description_Walker()) ); ?>
					

					<a href="#" class="mobile-trigger burger-trigger">
						<span></span>
						<span></span>
						<span></span>
					</a>
				
				</nav><!-- #site-navigation -->

				<nav class="columns-12 mobile-nav-container">

					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'items_wrap' => '<ul id="mobile-nav" class="nav menu">%3$s</ul>', 'container_class' => 'menu-mobile-menu-container', ) ); ?>

				</nav>
			
			</div>

		</div>

	</header><!-- #masthead -->

	<section id="main" class="site-main">
