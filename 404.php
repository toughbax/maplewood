<?php
/*
 * This is the template that displays on all pages by default.
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">


				<article id="post-<?php the_ID(); ?>" <?php post_class('error-404'); ?>>

				<!-- 	<header class="entry-header">
						
						<h2 class="entry-title"><?php _e( 'Error! Error!', 'anvil' ); ?></h2>
					
					</header> --><!-- .entry-header -->

					<div class="entry-content">
					
						<p><?php _e( 'The page you are looking may have moved. Please select another page from the main navigation.','anvil'); ?></p>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->


		</div><!-- #content -->

	</div>
		
<?php get_footer(); ?>
