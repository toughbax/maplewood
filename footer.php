<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the id=main div and all content after
 */
?>

	</section><!-- #main -->

	<footer id="site-footer" role="contentinfo">
		<div class="scroll-up">
			<a href="#top-banner" class=""></a>
		</div>
		<div class="footer-wrap">

			<div class="row">
				<div class="columns-3">							
					<?php $footer_logo = get_field('footer_logo','options'); ?>
					<a href="<?php the_field('footer_logo_link','options'); ?>">
						<img src="<?php echo $footer_logo['url']; ?>" alt="Footer Logo">
					</a>
				</div>
				<div class="columns-9">					
					<small><?php the_field('copyright','options'); ?></small>
				</div>
			</div>
				
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>
</body>
</html>
