<?php 

// FUNCTIONS RELATING TO NEIGHBOURHOOD MAP

function fs_get_neighbourhood_locations(){
	// get locaiton terms
	$location_terms = get_terms('location-types', 'orderby=id&hide_empty=0');
	$location_count = 0; 
	//empty array for locations
	$locations_array;
	$current_count = 0;

	//loop through location terms
	foreach( $location_terms as $term ){

		//location term info (name / slug)
		$locations_array[$current_count]['term'] = $term;
		$locations_array[$current_count]['term_name'] = $term->name;		
		$locations_array[$current_count]['term_slug'] = $term->slug;
		$locations_array[$current_count]['locations'] = Array();


		//get all locations for this location type
		$args = array(
			'post_type' => 'locations',
			'posts_per_page' => -1,
			'order' 	=> 'ASC',
			'orderby'	=> 'title',
			'tax_query' => array(
				array(
					'taxonomy' => 'location-types',
					'field' => 'slug',
					'terms' => $term->slug
				)
			)
		); 
		$location_query = new WP_Query($args);
		$post_count = 0;
		if($location_query->have_posts()): while($location_query->have_posts()): $location_query->the_post(); ?>
			
			<?php 
			//set current location name/count/location (long + lat)
			$locations_array[$current_count]['locations'][$post_count]['name'] = get_the_title(); 
			$locations_array[$current_count]['locations'][$post_count]['count'] = $location_count++;
			$locations_array[$current_count]['locations'][$post_count]['location'] = get_field('location');

			$post_count++;
			
			
		endwhile; endif; wp_reset_query();

		$current_count++;


	} 
	return $locations_array;
}
