<?php 
	/*	OPTIONS_PAGE.PHP

		This page should be used to register extra options pages for
		advanced custom fields.

		EXAMPLE:

		register_options_page('Name');
		
	*/

 // 	if(function_exists("register_options_page"))
	// {
	// 	register_options_page('Site Options');
	// 	register_options_page('Header');
	// 	register_options_page('Footer');
	// }
	if(function_exists('acf_add_options_page')) { 
	 
		acf_add_options_page();
			acf_add_options_sub_page('Site Options');
			acf_add_options_sub_page('Header');
			acf_add_options_sub_page('Footer');
	 
	}
	/*   END OF OPTIONS_PAGE.PHP   */

?>
