<?php
/*
 * This is the template that displays on all pages by default.
 */
get_header(); ?>

	<div class="row content-area">

		<div id="content" class="columns-12 site-content" role="main">
			<div class="page-wrap">
			<?php while ( have_posts() ) : the_post(); ?>
				

				<?php if(get_field('has_banner') == TRUE ) { get_template_part( 'templates/content', 'banner' ); } ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('page-content'); ?>>

					<div class="entry-content">
					
						<?php the_field('content'); ?>

						<?php if(get_field('page_button_text') && get_field('page_button_link')): ?>
							<a href="<?php the_field('page_button_link'); ?>" target="_blank" class="page-button"><?php the_field('page_button_text'); ?></a>
						<?php elseif(get_field('page_button_text') && !get_field('page_button_link')): ?>
							<a class="page-button"><?php the_field('page_button_text'); ?></a>				
						<?php endif; ?>

					</div><!-- .entry-content -->

				</article><!-- #post-## -->
				
				<?php if(get_field('links')): ?>
					<div id="link-repeater">
						<ul class="block-grid-2">
							<?php while(have_rows('links')): the_row(); ?>
								<li>
									<?php $image = get_sub_field('image'); ?>
									<?php if(get_sub_field('link')): ?>
										<a href="<?php the_sub_field('link'); ?>" target="_blank">
									<?php endif; ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('title'); ?>">
										<span class="link-title"><?php the_sub_field('title'); ?></span>
									<?php if(get_sub_field('link')): ?>	
										</a>
									<?php endif; ?>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>

				<?php if(get_field('bottom_image')): ?>
					<div id="bottom-image">
						<?php $image = get_field('bottom_image'); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php the_sub_field('title'); ?>">								
					</div>
				<?php endif; ?>

			<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- #content -->

	</div>
		
<?php get_footer(); ?>
